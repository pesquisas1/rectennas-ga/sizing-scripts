%Fonte: Alsager, 2011 [Adaptado para MATLAB R2015a]
%Program to calculate the parameters to design a rectangular patch antenna
%the user have to feed the values of frequency, dielectric constant, and
%height of the dielectric.
%the program will calculate automatically the width and length of the patch
%and the distance to the feed point

function[] = cal
global k0 W L
f = input( 'input frequency f in Ghz: ');
Er = input ( 'input dielectric constant of the substrate Er ');
h = input( 'input height of substrate h in mm: ');
h=h/1000; %turns height to meters
f=f*1e9; % turn frequency to HZ
c = 3e8; % speed of light
k0=2*pi*f/c; %wave number
Rin = 50; %required input impedance of the antenna

% calculating Width and Length of the Patch

W = ( c / ( 2 * f ) ) * ( ( 2 / ( Er + 1 ) )^0.5);
Er_eff = (Er+1)/2 + (( Er -1 )/2)*(1/(sqrt(1+(12*(h/W)))));
L_eff = c/(2*f*sqrt(Er_eff));
a1 = ( Er_eff + 0.3 ) * ( ( W / h ) + 0.264 );
a2 = ( Er_eff - 0.258 ) * ( ( W / h ) + 0.8 );
delta_L = (0.412 * ( a1 / a2 )) * h;
L = L_eff - 2*delta_L;

% calculating the distance of the inset feed point

t = 0:pi;
g1(t);
I1 = quad(@g1,0,pi);
G1 = I1/(120*pi*pi);
g12(t);
I12 = quad(@g12,0,pi);
G12 = I12/(120*pi*pi);
yo = (L/pi)*(acos(sqrt(2*Rin*(G1+G12))));
str=['width = ', num2str(W*1000), ' mm']
str=['length = ', num2str(L*1000), ' mm']
str=['the inset feed point distance = ', num2str(yo*1000), ' mm']
h=h/100;

%subfunktions

function [f] = g1(t)
global k0 W
f = ((sin(k0*W*0.5*cos(t))/cos(t)).^2*(sin(t)).^3);
function [k] = g12(t)
global k0 W L
k=(((sin(k0*W*0.5*cos(t))/cos(t)).^2)*(sin(t).^3)).*besselj(0,k0*L*sin(t));
import numpy as np
import scipy.constants as cte, scipy.integrate as integral
from scipy.special import jv as besselj 

def CalculaInsetFeed(fr, L1, L2, Rin):

    k0 = 2*cte.pi*fr*np.sqrt(cte.epsilon_0*cte.mu_0) #Vacuum wave number
    I1_func = lambda x: ((np.sin(k0*L1*np.cos(x)/2)/np.cos(x))**2) * np.sin(x)**3
    I1 = integral.quad(I1_func, 0, np.pi)[0]
    G1 = I1/(120*cte.pi**2)

    G12_func = lambda x: I1_func(x) * besselj(0, k0*L2*np.sin(x))
    G12 = 1/(120*cte.pi**2) * integral.quad(G12_func, 0, np.pi)[0]

    y0 = (L2/cte.pi)*np.arccos(np.sqrt(2*Rin*(G1+G12)))

    return y0

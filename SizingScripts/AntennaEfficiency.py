#Autor: Cícero Matheus, 2019
#Equações de Pozar, 2011

gain = float(input("Gain [dB]: "))
directivity = float(input("Directivity [dBi]: "))

gain = 10**(gain/10)
directivity = 10**(directivity/10)

print("Efficiency = %.4f %%" %(gain/directivity * 100))
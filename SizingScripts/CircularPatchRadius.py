#Autor: Cícero Matheus, 2018
#Equações de Balanis, 2005

import numpy as np 

fr = float(input("Type the desired frequency in [Hz]: "))
er = float(input("Type the substrate's relative permittivity: "))
d = float(input("Type the substrate's height [mm]: "))

F = (8.791e9)/(fr*np.sqrt(er))
a = F/np.sqrt((1+ (2*d)/(np.pi*er*F) * (np.log((np.pi*F)/(2*d)) + 1.7726)))

print("a = %.04f [cm]" % (a))

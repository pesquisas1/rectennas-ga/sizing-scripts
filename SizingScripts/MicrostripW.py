#Autor: Cícero Matheus, 2018
#Equações de Pozar, 2011

import numpy as np

z0 = float(input("Type the line's characteristic impedance [ohms]:"))
er = float(input("Type the substrate's relative permittivity:"))
d = float(input("Type the substrate's height [mm]:"))

A = z0/60 * np.sqrt((er+1)/2) + (er-1)/(er+1) * (0.23 + 0.11/er)
B = (377*np.pi)/(2*z0*np.sqrt(er))

ratio = (8*np.exp(A))/(np.exp(2*A) - 2)

if ratio < 2:
    print("W = %.4f [mm]" %(ratio*d))
else:
    ratio = 2/np.pi * (B - 1 - np.log(2*B - 1) + (er-1)/(2*er)*(np.log(B-1) + 0.39 - 0.61/er))
    print("W = %.4f [mm]" %(ratio*d))
    

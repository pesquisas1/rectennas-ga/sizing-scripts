#Autor: Cícero Matheus, 2018
#Adaptado de Alsager, 2011

import numpy as np
import scipy.constants as cte, scipy.integrate as integral
import Libs.InsetFeed as IF

fr = float(input("Type the desired frequency in [Hz]: "))
er = float(input("Type the substrate's relative permittivity: "))
d = float(input("Type the substrate's height [mm]: "))
Rin = float(input("Type the patch's impedance [ohms]: "))
d /= 1000 # Convert to meters

#Patch Sizing
L1 = cte.c/(2*fr) * np.sqrt(2/(er+1))
eeff = (er+1)/2 + (er-1)/2 * 1/np.sqrt(1 + 12*d/L1)
deltaL = 0.412*d* ((eeff +0.3)*(L1/d + 0.264))/((eeff - 0.258)*(L1/d + 0.8))
L2 = 1/(2*fr*np.sqrt(eeff*cte.mu_0*cte.epsilon_0)) - 2*deltaL

#Impedance Matching
y0 = IF.CalculaInsetFeed(fr, L1, L2, Rin)

print("L1 = %.04f [mm]" % (L1*1000))
print("L2 = %.04f [mm]" % (L2*1000))
print("y0 = %.4f [mm]" %(y0*1000))





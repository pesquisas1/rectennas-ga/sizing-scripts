#Autor: Cícero Matheus, 2019

import matplotlib.pyplot as plt 
import os, glob

#Variáveis
os.chdir("./5/")
arquivos = glob.glob("*.txt")
bandas = {} #Bandas de Cada Estrutura
s11_28 = {} #S11 em 28 [GHz] de Cada Estrutura

def ler_dados(arquivo):
    fp = open(arquivo, "r")
    dados = fp.readlines()
    fp.close()

    frequencia = []
    s11 = []

    for dado in dados:
        dado = dado.split()
        frequencia.append( float(dado[0]))
        s11.append(float(dado[1]))

    return frequencia, s11

def retornar_bandas(frequencia, s11):
    bandas = []
    temp = []
    for i in range(0, len(s11)):
        if(s11[i] < -10):
            if(len(temp) == 1):
                continue
            else:
                temp.append(frequencia[i])
        else:
            if(len(temp) == 0):
                continue
            else:
                temp.append(frequencia[i])
                bandas.append(temp)
                temp = []
    return bandas

def retornar_s11_28(frequencia, s11):
    stepsize = frequencia[1] - frequencia[0]
    inicio = frequencia[0]

    ind_inicio = round(inicio/stepsize)
    ind_fim = round(28/stepsize)

    return s11[ind_fim - ind_inicio]


#Configurar Plots
plt.rcParams['font.size'] = 30
figsize = (20, 13)
dpi = 128

#Plotar
for arquivo in arquivos:
    titulo = arquivo.split(".")[0]
    print("Gerando Gráfico de " + titulo)

    frequencia, s11 = ler_dados(arquivo)
    figura = plt.figure(figsize=figsize, dpi=dpi)

    plt.plot(frequencia, s11, "k-", linewidth=5)
    plt.title("Perda por Retorno - "+titulo)
    plt.xlabel("Frequência em GHz")
    plt.ylabel("Perda por Retorno em dB")
    plt.grid(True, color="black")
    plt.savefig("./Saída/" + titulo + ".png")

    bandas[titulo] = retornar_bandas(frequencia, s11)
    s11_28[titulo] = retornar_s11_28(frequencia, s11)

    print("Gráfico Gerado!")

print("Gerando Relatório")

fp = open("./Saída/relatório.txt", "w")

for chave in bandas.keys():
    fp.write(".:%s\n" % (chave.upper()))
    for banda in bandas[chave]:
        fp.write("%.04f | %.04f\n" %(banda[0], banda[1]))
    
    fp.write("RL(28) = %.04f\n\n" %(s11_28[chave]))

fp.close()
